package worldline.ssm.rd.ux.wltwitter.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Vivien on 26/01/2017.
 */

public class TweetsAdapters extends BaseAdapter {

    private List<Tweet> mTweets;
    private LayoutInflater mLayoutInflater;

    public TweetsAdapters(List<Tweet> tweets){
        this.mTweets = tweets;

        mLayoutInflater = LayoutInflater.from(WLTwitterApplication.getContext());
    }

    @Override
    public int getCount() {
        return null != mTweets ? mTweets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mTweets ? mTweets.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final View view = mLayoutInflater.inflate(R.layout.tweet_listitem, null);
        final Tweet tweet = (Tweet)getItem(position);

        final TextView userName = (TextView)view.findViewById(R.id.tweetListItemNameTextView);
        userName.setText(tweet.user.name);

        final TextView userAlias = (TextView)view.findViewById(R.id.tweetListItemAlisTextView);
        userAlias.setText("@" + tweet.user.screenName);

        final TextView textView = (TextView)view.findViewById(R.id.tweetListItemTweetTextView);
        textView.setText(tweet.text);

        return view;
    }

    
    private class ViewHolder {
        private TextView name;
        private TextView alias;
        private TextView text;

        public ViewHolder(View view){
            name = (TextView)view.findViewById(R.id.tweetListItemNameTextView);
            alias = (TextView)view.findViewById(R.id.tweetListItemAlisTextView);
            text = (TextView)view.findViewById(R.id.tweetListItemTweetTextView);
        }
    }
}

