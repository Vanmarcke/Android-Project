package worldline.ssm.rd.ux.wltwitter.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import worldline.ssm.rd.ux.wltwitter.Components.ImageMemoryCache;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;

/**
 * Created by Vivien on 29/01/2017.
 */

public class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

    private final ImageView imageView;

    private final ImageMemoryCache mImageMemoryCache;

    public DownloadImageAsyncTask(ImageView imageView, ImageMemoryCache mImageMemoryCache) {
        this.imageView = imageView;
        this.mImageMemoryCache = mImageMemoryCache;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        if ((null != params) && (params.length > 0)) {
            final String imageUrl = params[0];

            try {
                final Bitmap bitmap = TwitterHelper.getTwitterUserImage(imageUrl);

                if (null != mImageMemoryCache){
                    mImageMemoryCache.addBitmapToMemoryCache(imageUrl, bitmap);
                }
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        if ((null != bitmap) && (null != imageView)) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
