package worldline.ssm.rd.ux.wltwitter.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.Components.ImageMemoryCache;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.async.DownloadImageAsyncTask;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Vivien on 26/01/2017.
 */

public class TweetsAdapters extends BaseAdapter implements View.OnClickListener{

    private final List<Tweet> mTweets;
    private final LayoutInflater mLayoutInflater;
    private final ImageMemoryCache mImageMemoryCache;
    private TweetListener mListener;

    public TweetsAdapters(List<Tweet> tweets) {
        this.mTweets = tweets;

        mLayoutInflater = LayoutInflater.from(WLTwitterApplication.getContext());

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 16;
        mImageMemoryCache = new ImageMemoryCache(cacheSize);
    }

    public void setTweetListener(TweetListener listener){
        mListener = listener;
    }

    @Override
    public int getCount() {
        return null != mTweets ? mTweets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mTweets ? mTweets.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.tweet_listitem, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final View view = mLayoutInflater.inflate(R.layout.tweet_listitem, null);
        final Tweet tweet = (Tweet) getItem(position);

        holder.name.setText(tweet.user.name);

        holder.alias.setText("@" + tweet.user.screenName);

        holder.text.setText(tweet.text);

        holder.button.setTag(position);
        holder.button.setOnClickListener(this);

        final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);

        if (null == image) {
            new DownloadImageAsyncTask(holder.image, mImageMemoryCache).execute(tweet.user.profileImageUrl);
        } else {
            holder.image.setImageBitmap(image);
        }


        return convertView;
    }

    @Override
    public void onClick(View view) {
        int position = (Integer) view.getTag();
        System.out.println("bonjour");

        // If we have a listener set, call the retweet method
        if (null != mListener){
            final Tweet tweet = (Tweet) getItem(position);
            mListener.onRetweet(tweet);
        }
    }


    private class ViewHolder {
        private TextView name;
        private TextView alias;
        private TextView text;
        public ImageView image;
        public Button button;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.tweetListItemNameTextView);
            alias = (TextView) view.findViewById(R.id.tweetListItemAliasTextView);
            text = (TextView) view.findViewById(R.id.tweetListItemTweetTextView);
            image = (ImageView) view.findViewById((R.id.tweetListItemImageView));
            button = (Button) view.findViewById(R.id.tweetListItemButton);
        }
    }
}

