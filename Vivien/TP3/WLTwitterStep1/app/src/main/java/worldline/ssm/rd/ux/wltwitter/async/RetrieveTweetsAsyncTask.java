package worldline.ssm.rd.ux.wltwitter.async;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.ui.fragments.TweetsFragment;

/**
 * Created by Vivien on 19/01/2017.
 */

public class RetrieveTweetsAsyncTask extends android.os.AsyncTask<String, Integer, List<Tweet>>{

    private TweetChangeListener mListener;

    public RetrieveTweetsAsyncTask(TweetChangeListener mListener){
        this.mListener = mListener;
    }


    @Override
    protected List<Tweet> doInBackground(String... params) {

        String login = params[0];

        if (login != null){
            List<Tweet> tweetList = TwitterHelper.getTweetsOfUser(login);
            return tweetList;
        }

        return TwitterHelper.getFakeTweets();
    }

    @Override
    protected void onPostExecute(List<Tweet> tweetList){
        super.onPostExecute(tweetList);

        if (null != mListener){
            mListener.onTweetRetrieved(tweetList);
        }

//        for(Tweet tweet: tweetList){
//            log.d("["+ R.string.app_name+"]"+tweet.text);
//        }

    }

}
