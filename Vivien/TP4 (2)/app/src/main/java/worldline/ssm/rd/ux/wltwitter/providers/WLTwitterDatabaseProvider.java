package worldline.ssm.rd.ux.wltwitter.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseHelper;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by Vivien on 02/02/2017.
 */

public class WLTwitterDatabaseProvider extends ContentProvider {
    private static final int TWEET_CORRECT_URI_CODE = 42;
    UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    WLTwitterDatabaseHelper mDBHelper = new WLTwitterDatabaseHelper(getContext());

    @Override
    public boolean onCreate() {

        uriMatcher.addURI(WLTwitterDatabaseContract.CONTENT_PROVIDER_TWEETS_AUTHORITY, WLTwitterDatabaseContract.TABLE_TWEETS, TWEET_CORRECT_URI_CODE);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri arg0, String[] arg1, String arg2, String[] arg3, String arg4){
        Log.v(Constants.General.LOG_TAG, "QUERY");
        return mDBHelper.getReadableDatabase().query(WLTwitterDatabaseContract.TABLE_TWEETS, arg1, arg2, arg3, arg4, null, null);
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        if (uriMatcher.match(uri) == TWEET_CORRECT_URI_CODE){
            return WLTwitterDatabaseContract.TWEETS_CONTENT_TYPE;
        }
        throw new IllegalArgumentException("Unknown URI" + uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.i(Constants.General.LOG_TAG, "INSERT");
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.e(Constants.General.LOG_TAG, "DELETE");
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(Constants.General.LOG_TAG, "UPDATE");
        return 0;
    }
}
