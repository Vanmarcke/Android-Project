package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.PreferenceUtils;

/**
 * Created by Vivien on 12/01/2017.
 */

public class WLTwitterLoginActivity extends Activity implements View.OnClickListener {

    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private static Context sContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        sContext = getApplicationContext();

        mLoginEditText = (EditText) findViewById(R.id.loginEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);

        final String storedLogin = PreferenceUtils.getLogin();
        final String storedPassword = PreferenceUtils.getPassword();

        if (!TextUtils.isEmpty(storedLogin) && (!TextUtils.isEmpty(storedPassword))) {
            final Intent homeIntent = getHomeActivityIntent(storedLogin);
            startActivity(homeIntent);

        }
    }

    @Override
    public void onClick(View view) {
        if (TextUtils.isEmpty(mLoginEditText.getText())) {
            Toast.makeText(this, R.string.error_no_login, Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(mPasswordEditText.getText())) {
            Toast.makeText(this, R.string.error_no_password, Toast.LENGTH_LONG).show();
            return;
        }

        final Intent homeIntent = getHomeActivityIntent(mLoginEditText.getText().toString());
        PreferenceUtils.setLogin(mLoginEditText.getText().toString());
        PreferenceUtils.setPassword(mPasswordEditText.getText().toString());
        startActivity(homeIntent);
    }

    private Intent getHomeActivityIntent(String userName) {
        Intent intent = new Intent(this, WLTwitterHome.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.Login.EXTRA_LOGIN, userName);
        intent.putExtras(extras);
        return intent;
    }

    public static Context getContext() {
        return sContext;
    }
}
