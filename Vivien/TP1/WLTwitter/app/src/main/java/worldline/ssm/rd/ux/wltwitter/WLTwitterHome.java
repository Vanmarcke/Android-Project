package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.PreferenceUtils;

/**
 * Created by Vivien on 12/01/2017.
 */

public class WLTwitterHome extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intent = getIntent();

        if(null != intent){
            final Bundle extras = intent.getExtras();

            if(null != extras && extras.containsKey(Constants.Login.EXTRA_LOGIN)){
                final String login = extras.getString(Constants.Login.EXTRA_LOGIN);
                getActionBar().setTitle(login);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.wltwitter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.actionLogout) {

            PreferenceUtils.setLogin(null);
            PreferenceUtils.setPassword(null);

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
