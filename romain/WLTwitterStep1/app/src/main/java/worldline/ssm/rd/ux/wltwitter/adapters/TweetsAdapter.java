package worldline.ssm.rd.ux.wltwitter.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.Components.ImageMemoryCache;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.async.DownloadImageAsyncTask;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Romain on 26/01/2017.
 */

public class TweetsAdapter extends BaseAdapter {

    private final List<Tweet> mTweets;

    private LayoutInflater mLayoutInflater;
    private final ImageMemoryCache mImageMemoryCache;

    public TweetsAdapter(List<Tweet> tweets) {
        this.mTweets = tweets;
        this.mLayoutInflater = LayoutInflater.from(WLTwitterApplication.getContext()) ;

        final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
        final int cacheSize = maxMemory/16;
        mImageMemoryCache = new ImageMemoryCache(cacheSize);
    }


    @Override
    public int getCount() {
        return null != mTweets ? mTweets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mTweets ? mTweets.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.tweet_listitem, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }


        final Tweet tweet = (Tweet) getItem(position);


        holder.name.setText(tweet.user.name);

        holder.alias.setText("@" + tweet.user.screenName);

        holder.text.setText(tweet.text);

        final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);

        if (image == null) {
            new DownloadImageAsyncTask(holder.image, null).execute(tweet.user.profileImageUrl);
        } else {
            holder.image.setImageBitmap(image);
        }



        return convertView;
    }

    private class ViewHolder {
        private TextView name;
        private TextView alias;
        private TextView text;
        private ImageView image;

        public ViewHolder(View view) {
            name = (TextView)view.findViewById(R.id.tweetListItemNameTextView);
            alias = (TextView)view.findViewById(R.id.tweetListItemAliasTextView);
            text = (TextView)view.findViewById(R.id.tweetListItemTweetTextView);
            image = (ImageView) view.findViewById(R.id.tweetListItemImageView);


        }
    }
}
