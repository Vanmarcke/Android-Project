package worldline.ssm.rd.ux.wltwitter.async;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Romain on 19/01/2017.
 */

public class RetrieveTweetsAsyncTask extends AsyncTask<String, Void, List<Tweet>> {

    private TweetChangeListener mListener;

    public RetrieveTweetsAsyncTask(TweetChangeListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected List<Tweet> doInBackground(String... params ) {
        String login = params[0];
        if (!login.isEmpty() && (params.length > 0)) {
            return TwitterHelper.getTweetsOfUser(login);
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        super.onPostExecute(tweets);

        if(null != mListener)
            mListener.onTweetRetrieved(tweets);

//        for (Tweet tweet: tweets) {
//            Log.d("TweetAsyncTask", tweet.text);
//        }
    }
}
