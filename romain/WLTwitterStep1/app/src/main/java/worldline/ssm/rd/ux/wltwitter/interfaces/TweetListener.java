package worldline.ssm.rd.ux.wltwitter.interfaces;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Romain on 25/01/2017.
 */

public interface TweetListener {
    public void onRetweet(Tweet tweet);
    public void onViewTweet(Tweet tweet);
}
