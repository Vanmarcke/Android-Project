package worldline.ssm.rd.ux.wltwitter.Components;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Romain on 30/01/2017.
 */

public class ImageMemoryCache {

    private LruCache<String, Bitmap> mMemoryCache;

    public ImageMemoryCache(int maxCacheSize) {
        mMemoryCache = new LruCache<String, Bitmap>(maxCacheSize) {

            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return (bitmap.getRowBytes()* bitmap.getHeight()) /1024;
            }
        };
    }

    public void addBitMapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
}
